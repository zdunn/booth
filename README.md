# Booth

Booth is a personal, social CMS following the tenets of the [IndieWeb](https://indieweb.org/IndieWeb). A Booth site serves as your online identity and allows you to interact with any other IndieWeb capable site. Interaction between your site and other IndieWeb sites are direct interactions that don't depend on corporate social networks. You can use Booth for microblogging (like Twitter), macroblogging (like Medium), and image sharing (like Instagram), but you own the data.

You can post to your Booth site using any [MicroPub](https://indieweb.org/micropub) client. This includes:

* [Quill](https://quill.p3k.io)
* [Together](https://http://alltogethernow.io/)
* [Micropublish](https://micropublish.net/)
* [Omnibear](https://omnibear.com/)
* and many others that can be found [here](https://indieweb.org/Micropub/Clients)

## Planned Features

1. Internal handling of webmentions. Booth currently uses external services for sending/receiving webmentions.
1. [MicroSub](https://indieweb.org/Microsub) server support so you can follow public blogs and other IndieWeb users.
1. A client that supports MicroSub and MicroPub so you have all your follows and interactions in one app.
