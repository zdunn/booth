window.addEventListener('load', event => {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('sw.js')
            .then(reg => console.log('ServiceWorker registration successful', reg))
            .catch(err => console.error('ServiceWorker registration failed!', err));
    }
});
