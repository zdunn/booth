const SW_VERSION = '0.0.1';
const CACHE_NAME = `cueball-cache-${SW_VERSION}`;
const ITEMS_TO_CACHE = [
    'favicon.ico',
    'css/index.css'
];

// install is sent when the service worker is being installed
self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(cache => cache.addAll(ITEMS_TO_CACHE))
    );
});

// activate is sent when the service worker has been registered and installed.
// Clean up an older version of the service worker here.
self.addEventListener('activate', event => {
});

// fetch is sent whenever a page in the service worker scope requires a network resource
self.addEventListener('fetch', event => {
    event.respondwith(
        caches.match(event.request)
            .then(res => response ? response : fetch(event.request))
    );
});

// push is invoked by the Push API when a new push event is received
self.addEventListener('push', event => {
    console.log('Push event', event);
});

// sync is sent if the browser previously detected that the connection was unavailable
// and now signals the service worker that the internet connection is working
self.addEventListener('sync', event => {
    console.log('Sync event', event);
});

