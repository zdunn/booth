use crate::{micropub::post::PostType, settings::Settings, site::Site, utils};
use rocket::{get, routes, Route, State};
use rocket_contrib::templates::Template;

pub fn routes() -> Vec<Route> {
    routes![boosts, boosts_index_for_page, get_boost_with_slug]
}

#[get("/")]
fn boosts(settings: Settings, site: State<Site>) -> Template {
    boosts_index_for_page(1, settings, site)
}

#[get("/?<page>")]
fn boosts_index_for_page(page: usize, settings: Settings, site: State<Site>) -> Template {
    let posts = utils::load_entries_by_type(PostType::Boost, page, &site);

    site.view()
        .title("Boosts")
        .description(&format!("{}'s Boosts", &site.h_card.name))
        .slug("boosts")
        .posts(posts)
        .settings(settings)
        .render("feeds/boosts")
}

#[get("/<slug>")]
fn get_boost_with_slug(slug: String, settings: Settings, site: State<Site>) -> Template {
    let boost = utils::load_entry_by_slug(&slug, &site);

    site.view()
        .title("Boost")
        .slug(&format!("boosts/{}", &slug))
        .post(boost)
        .settings(settings)
        .render("posts/boost")
}
