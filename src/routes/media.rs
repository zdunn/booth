use crate::micropub::responders::MicropubResponse;
use rocket::{post, routes, Route};

pub fn routes() -> Vec<Route> {
    routes![upload_media]
}

#[post("/media")]
fn upload_media() -> MicropubResponse {
    // TODO: Add a media parameter, store media in &site.content_dir
    // and return MicropubResponse::Created(url)

    MicropubResponse::NotImplemented
}
