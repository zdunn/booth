use crate::{micropub::post::PostType, settings::Settings, site::Site, utils};
use rocket::{get, routes, Route, State};
use rocket_contrib::templates::Template;

pub fn routes() -> Vec<Route> {
    routes![articles_index, articles_index_for_page, get_article_with_slug]
}

#[get("/")]
fn articles_index(settings: Settings, site: State<Site>) -> Template {
    articles_index_for_page(1, settings, site)
}

#[get("/?<page>")]
fn articles_index_for_page(page: usize, settings: Settings, site: State<Site>) -> Template {
    let posts = utils::load_entries_by_type(PostType::Article, page, &site);

    site.view()
        .title("Articles")
        .description(&format!("{}'s Articles", &site.h_card.name))
        .slug("articles")
        .posts(posts)
        .settings(settings)
        .render("feeds/articles")
}

#[get("/<slug>")]
fn get_article_with_slug(slug: String, settings: Settings, site: State<Site>) -> Template {
    let article = utils::load_entry_by_slug(&slug, &site);

    site.view()
        .title(&article.name)
        .slug(&format!("articles/{}", &slug))
        .post(article)
        .settings(settings)
        .render("posts/article")
}
