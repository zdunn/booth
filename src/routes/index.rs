use crate::{micropub::post::PostView, settings::Settings, site::Site, utils};
use rocket::{
    get,
    http::{ContentType, Cookie, Cookies},
    post,
    request::Form,
    response::{content::Content, Redirect},
    routes, Route, State,
};
use rocket_contrib::templates::Template;
use serde_json::json;

pub fn routes() -> Vec<Route> {
    routes![
        index,
        index_for_page,
        get_tag,
        get_hcard_json,
        web_manifest,
        about,
        resume,
        get_settings,
        post_settings,
    ]
}

#[get("/")]
fn index(settings: Settings, site: State<Site>) -> Template {
    index_for_page(1, settings, site)
}

#[get("/?<page>")]
fn index_for_page(page: usize, settings: Settings, site: State<Site>) -> Template {
    let posts = utils::load_entries(page, &site);

    site.view()
        .title("Index")
        .slug("")
        .posts(posts)
        .settings(settings)
        .render("feeds/index")
}

#[get("/tags/<tag>")]
fn get_tag(tag: String, settings: Settings, site: State<Site>) -> Template {
    let posts: Vec<PostView> = utils::load_entries(1, &site)
        .into_iter()
        .filter(|p| p.tags.contains(&tag))
        .collect();

    site.view()
        .title(&format!("{} - Tag Results", &tag))
        .slug(&format!("tags/{}", &tag))
        .posts(posts)
        .settings(settings)
        .insert("tag", &tag)
        .render("feeds/tags")
}

#[get("/hcard.json")]
fn get_hcard_json(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "json");
    let doc = json!({
        "items": &site.h_card
    })
    .to_string();

    Content(content_type, doc)
}

#[get("/manifest.json")]
fn web_manifest(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "manifest+json");
    let doc = json!({
        "name": &site.name,
        "short_name": &site.short_name,
        "description": &site.description,
        "start_url": String::from("/"),
        "display": String::from("standalone"),
        "background_color": String::from("#000000"),
        "theme_color": String::from("#434279"),
        "homepage_url": &site.h_card.url,
        "lang": &site.locale,
        "scope": String::from("/"),
        "icons": [
            {
                "src": format!("{}/images/icon-512-512.png", &site.public_dir),
                "sizes": "512x512"
            },
            {
                "src": format!("{}/images/icon-192-192.png", &site.public_dir),
                "sizes": "192x192"
            },
            {
                "src": format!("{}/images/icon-144-144.png", &site.public_dir),
                "sizes": "144x144"
            },
            {
                "src": format!("{}/images/icon-96-96.png", &site.public_dir),
                "sizes": "96x96"
            },
            {
                "src": format!("{}/images/icon-72-72.png", &site.public_dir),
                "sizes": "72x72"
            },
            {
                "src": format!("{}/images/icon-48-48.png", &site.public_dir),
                "sizes": "48x48"
            }
        ]
    })
    .to_string();

    Content(content_type, doc)
}

#[get("/about")]
fn about(settings: Settings, site: State<Site>) -> Template {
    site.view()
        .title("About Me")
        .slug("about")
        .settings(settings)
        .render("about")
}

#[get("/resume")]
fn resume(settings: Settings, site: State<Site>) -> Template {
    site.view()
        .title(&site.h_card.name)
        .slug("resume")
        .settings(settings)
        .render("resume")
}

#[get("/settings")]
fn get_settings(settings: Settings, site: State<Site>) -> Template {
    site.view()
        .title("Settings")
        .slug("settings")
        .settings(settings)
        .render("settings")
}

#[post("/settings", data = "<settings>")]
fn post_settings(mut cookies: Cookies, settings: Form<Settings>) -> Redirect {
    let theme = Cookie::build("theme", settings.into_inner().theme).permanent().finish();
    cookies.add(theme);
    Redirect::to("/settings")
}
