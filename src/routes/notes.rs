use crate::{micropub::post::PostType, settings::Settings, site::Site, utils};
use rocket::{get, routes, Route, State};
use rocket_contrib::templates::Template;

pub fn routes() -> Vec<Route> {
    routes![notes_index, notes_index_for_page, get_note_with_slug]
}

#[get("/")]
fn notes_index(settings: Settings, site: State<Site>) -> Template {
    notes_index_for_page(1, settings, site)
}

#[get("/?<page>")]
fn notes_index_for_page(page: usize, settings: Settings, site: State<Site>) -> Template {
    let posts = utils::load_entries_by_type(PostType::Note, page, &site);

    site.view()
        .title("Notes")
        .description(&format!("{}'s Notes", &site.h_card.name))
        .slug("notes")
        .posts(posts)
        .settings(settings)
        .render("feeds/notes")
}

#[get("/<slug>")]
fn get_note_with_slug(slug: String, settings: Settings, site: State<Site>) -> Template {
    let note = utils::load_entry_by_slug(&slug, &site);

    site.view()
        .title(&note.name)
        .slug(&format!("notes/{}", &slug))
        .post(note)
        .settings(settings)
        .render("posts/note")
}
