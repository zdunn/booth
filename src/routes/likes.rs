use crate::{micropub::post::PostType, settings::Settings, site::Site, utils};
use rocket::{get, routes, Route, State};
use rocket_contrib::templates::Template;

pub fn routes() -> Vec<Route> {
    routes![likes, likes_index_for_page, get_like_with_slug]
}

#[get("/")]
fn likes(settings: Settings, site: State<Site>) -> Template {
    likes_index_for_page(1, settings, site)
}

#[get("/?<page>")]
fn likes_index_for_page(page: usize, settings: Settings, site: State<Site>) -> Template {
    let posts = utils::load_entries_by_type(PostType::Like, page, &site);

    site.view()
        .title("Likes")
        .description(&format!("{}'s Likes", &site.h_card.name))
        .slug("likes")
        .posts(posts)
        .settings(settings)
        .render("feeds/likes")
}

#[get("/<slug>")]
fn get_like_with_slug(slug: String, settings: Settings, site: State<Site>) -> Template {
    let like = utils::load_entry_by_slug(&slug, &site);

    site.view()
        .title("Like")
        .slug(&format!("likes/{}", &slug))
        .post(like)
        .settings(settings)
        .render("posts/like")
}
