use crate::{micropub::post::PostView, settings::Settings, site::Site, utils};
use rocket::{get, http::ContentType, response::content::Content, routes, Route, State};
use rocket_contrib::templates::Template;

pub fn routes() -> Vec<Route> {
    if cfg!(feature = "search") {
        routes![search, open_search_description]
    }
    else {
        Vec::new()
    }
}

#[get("/search?<query>")]
fn search(query: String, settings: Settings, site: State<Site>) -> Template {
    let search_results: Vec<PostView> = utils::load_entries(1, &site)
        .into_iter()
        .filter(|p| {
            let name_matches = p.name.contains(&query);
            let content_matches = p.content.contains(&query);
            let summary_matches = p.summary.as_ref().map_or(false, |s| s.contains(&query));
            name_matches || content_matches || summary_matches
        })
        .collect();

    site.view()
        .title(&format!("{} - Search Results", &query))
        .slug(&format!("search/{}", &query))
        .posts(search_results)
        .settings(settings)
        .render("feeds/search-results")
}

#[get("/opensearch.xml")]
fn open_search_description(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "opensearchdescription+xml");
    let doc = format!(
        "<OpenSearchDescription xmlns=\"http://a9.com/-/spec/opensearch/1.1/\">
    <ShortName>{name}</ShortName>
    <Description>Search {name}</Description>
    <Image width=\"16\" height=\"16\" type=\"image/x-icon\">{public_dir}/favicon.ico</Image>
    <Url type=\"text/html\" method=\"get\" template=\"https://{domain}/search?query={{searchTerms}}\" />
</OpenSearchDescription>
",
        name = &site.name,
        public_dir = &site.public_dir,
        domain = &site.domain
    );

    Content(content_type, doc)
}
