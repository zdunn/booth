use crate::{
    micropub::{jf2, mf2, responders::MicropubResponse, syndication::SyndicationTarget, ConfigQuery},
    site::Site,
    utils,
    webmentions::WebmentionBuilder,
};
use log::debug;
use rocket::{
    get,
    http::ContentType,
    post,
    request::{Form, LenientForm},
    response::Content,
    routes, Route, State,
};
use rocket_contrib::json::Json;
use serde_json::json;
// use webmentions::WebmentionBuilder;

pub fn routes() -> Vec<Route> {
    routes![micropub_json_endpoint, micropub_form_endpoint, micropub_query_endpoint,]
}

#[post("/", format = "application/json", data = "<request_json>")]
fn micropub_json_endpoint(request_json: Json<jf2::EntryRequest>, site: State<Site>) -> MicropubResponse {
    let request = request_json.into_inner();
    debug!("Got request: {:#?}", request);

    let h_type = request
        .h
        .and_then(|mut v| v.pop())
        .unwrap_or_else(|| String::from("h-entry"));

    match h_type.as_ref() {
        "h-entry" => {
            let builder = site
                .post()
                .content(request.properties.content.and_then(|mut v| v.pop()))
                .name(request.properties.name.and_then(|mut v| v.pop()))
                .summary(request.properties.summary.and_then(|mut v| v.pop()))
                .tags(&mut request.properties.category.unwrap_or_else(Vec::new))
                .slug(request.properties.mp_slug.and_then(|mut v| v.pop()))
                .in_reply_to(request.properties.in_reply_to.and_then(|mut v| v.pop()))
                .repost_of(request.properties.repost_of.and_then(|mut v| v.pop()))
                .like_of(request.properties.like_of.and_then(|mut v| v.pop()))
                .photo(request.properties.photo.and_then(|mut v| v.pop()));
            let post_result = builder.validate();

            match post_result {
                Ok(builder) => {
                    let url = builder.url.clone();
                    // TODO: call build and store_post in a new thread; Return an Accepted HTTP response
                    let (post_view, links) = builder.build();
                    if let Some(mut targets) = links {
                        WebmentionBuilder::new(url.clone()).targets(&mut targets).send();
                    }
                    utils::store_post_view(&post_view, &site);
                    MicropubResponse::Created(url)
                },
                Err(e) => MicropubResponse::BadRequest(e.to_string()),
            }
        },
        unsupported_type => MicropubResponse::UnsupportedType(unsupported_type.to_string()),
    }
}

#[post("/", format = "application/x-www-form-urlencoded", data = "<request_form>")]
fn micropub_form_endpoint(request_form: LenientForm<mf2::EntryRequest>, site: State<Site>) -> MicropubResponse {
    let request = request_form.into_inner();
    debug!("Got request: {:#?}", request);

    let h_type = request.h.unwrap_or_else(|| String::from("entry"));
    match h_type.as_ref() {
        "entry" => {
            let mut tags = request
                .category
                .unwrap_or_else(String::new)
                .split(',')
                .map(|tag| tag.to_string())
                .collect();
            let builder = site
                .post()
                .content(request.content)
                .name(request.name)
                .summary(request.summary)
                .tags(&mut tags)
                .slug(request.mp_slug)
                .in_reply_to(request.in_reply_to)
                .repost_of(request.repost_of)
                .like_of(request.like_of)
                .photo(request.photo);
            let post_result = builder.validate();

            match post_result {
                Ok(builder) => {
                    let url = builder.url.clone();
                    // TODO: call build and store_post in a new thread; Return an Accepted HTTP response
                    let (post_view, links) = builder.build();
                    if let Some(mut targets) = links {
                        WebmentionBuilder::new(url.clone()).targets(&mut targets).send();
                    }
                    utils::store_post_view(&post_view, &site);
                    MicropubResponse::Created(url)
                },
                Err(e) => MicropubResponse::BadRequest(e.to_string()),
            }
        },
        unsupported_type => MicropubResponse::UnsupportedType(unsupported_type.to_string()),
    }
}

// TODO: Create MicropubResponse variants for the responses below.

#[get("/?<params..>")]
fn micropub_query_endpoint(params: Form<ConfigQuery>, site: State<Site>) -> Result<Content<String>, MicropubResponse> {
    // TODO: https://www.w3.org/TR/micropub/#configuration-p-1
    let query = params.into_inner();
    match query.q.as_ref() {
        "config" => Ok(query_config(&site)),
        "syndicate-to" => Ok(query_syndicate_to()),
        "source" => Ok(query_source(query.url)),
        "content" => Ok(query_content(query.url)),
        other => Err(MicropubResponse::UnsupportedQuery(other.to_string())),
    }
}

fn query_config(site: &Site) -> Content<String> {
    let content_type = ContentType::new("application", "json");

    let doc = json!({
        "media-endpoint": format!("{}/media", &site.h_card.url),
        "syndicate-to": syndication_targets()
    })
    .to_string();

    Content(content_type, doc)
}

fn query_source(url: Option<String>) -> Content<String> {
    let content_type = ContentType::new("application", "json");

    // TODO: https://www.w3.org/TR/micropub/#source-content-p-1
    let doc = url.unwrap_or_else(|| String::from("Source Content"));

    Content(content_type, doc)
}

fn query_content(url: Option<String>) -> Content<String> {
    let content_type = ContentType::new("application", "json");

    // stub
    let doc = url.unwrap_or_else(|| String::from("Content Request"));

    Content(content_type, doc)
}

fn query_syndicate_to() -> Content<String> {
    let content_type = ContentType::new("application", "json");

    let doc = json!({ "syndicate-to": syndication_targets() }).to_string();

    Content(content_type, doc)
}

fn syndication_targets() -> Vec<SyndicationTarget> {
    Vec::new()
}
