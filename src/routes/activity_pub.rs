use crate::{activity_pub::responders::APResponse, site::Site};
use rocket::{get, http::ContentType, post, response::Content, routes, Route, State};
use serde_json::json;

pub fn routes() -> Vec<Route> {
    routes![ap_actor, ap_inbox, incoming_message, ap_outbox, outgoing_message,]
}

#[get("/actor")]
fn ap_actor(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "activity+json");
    let doc = json!({
        "@context": [
            "https://www.w3.org/ns/activitystreams",
            { "@language": &site.locale }
        ],
        "id": &format!("https://{}/ap/actor", site.domain),
        "type": "Person",
        "inbox": format!("https://{}/ap/inbox", &site.domain),
        "outbox": format!("https://{}/ap/outbox", &site.domain),
        "following": format!("https://{}/ap/following", &site.domain),
        "followers": format!("https://{}/ap/followers", &site.domain),
        "preferredUsername": &site.h_card.nickname,
        "name": &site.h_card.name,
        "icon": format!("https://{}/{}", &site.domain, &site.avatar_src),
    })
    .to_string();

    Content(content_type, doc)
}

#[get("/inbox")]
fn ap_inbox() -> APResponse {
    APResponse::NotImplemented
}

#[post("/inbox")]
fn incoming_message() -> APResponse {
    APResponse::NotImplemented
}

#[get("/outbox")]
fn ap_outbox() -> APResponse {
    APResponse::NotImplemented
}

#[post("/outbox")]
fn outgoing_message() -> APResponse {
    APResponse::NotImplemented
}
