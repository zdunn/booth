use rocket::{
    get, post, routes, Route, State,
    http::ContentType,
    response::content::Content,
};
use serde_json::json;
use site::Site;

pub fn routes() -> Vec<Route> {
    routes![
        oembed_configuration
    ]
}

#[get("/")]
pub fn oembed_configuration(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "json");
    let doc = json!({
        "url_scheme": "",
        "api_endpoint": ""
    }).to_string();

    Content(content_type, doc)
}

