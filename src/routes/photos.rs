use crate::{micropub::post::PostType, settings::Settings, site::Site, utils};
use rocket::{get, routes, Route, State};
use rocket_contrib::templates::Template;

pub fn routes() -> Vec<Route> {
    routes![photos_index, photos_index_for_page, get_photo_with_slug]
}

#[get("/")]
fn photos_index(settings: Settings, site: State<Site>) -> Template {
    photos_index_for_page(1, settings, site)
}

#[get("/?<page>")]
fn photos_index_for_page(page: usize, settings: Settings, site: State<Site>) -> Template {
    let posts = utils::load_entries_by_type(PostType::Photo, page, &site);

    site.view()
        .title("Photos")
        .description(&format!("{}'s Photos", &site.h_card.name))
        .slug("photos")
        .posts(posts)
        .settings(settings)
        .render("feeds/photos")
}

#[get("/<slug>")]
fn get_photo_with_slug(slug: String, settings: Settings, site: State<Site>) -> Template {
    let photo = utils::load_entry_by_slug(&slug, &site);

    site.view()
        .title(&photo.name)
        .slug(&format!("photos/{}", &slug))
        .post(photo)
        .settings(settings)
        .render("posts/photo")
}
