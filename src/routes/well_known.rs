use crate::{site::Site, utils};
use rocket::{
    get,
    http::ContentType,
    request::Form,
    response::{status::NotFound, Content},
    routes, FromForm, Route, State,
};
use serde_json::json;

// check out https://github.com/rustodon/rustodon/blob/master/src/routes/well_known.rs
pub fn routes() -> Vec<Route> {
    routes![webfinger, host_meta, nodeinfo_jrd, nodeinfo]
}

#[derive(FromForm, Debug)]
pub struct WebFingerQuery {
    resource: String,
}

#[get("/.well-known/webfinger?<query..>")]
pub fn webfinger(query: Form<WebFingerQuery>, site: State<Site>) -> Result<Content<String>, NotFound<String>> {
    // validate that query contains the username in config
    let handle = format!(
        "{username}@{domain}",
        username = &site.h_card.nickname,
        domain = &site.domain
    )
    .to_lowercase();
    let query_acct = match &query.resource[..5] {
        "acct:" => &query.resource[5..],
        _ => &query.resource,
    }
    .to_lowercase();
    if query_acct == handle {
        let content_type = ContentType::new("application", "jrd+json");
        let doc = json!({
            "subject": format!("acct:{}", handle),
            "links": [
                {
                    "rel": "http://webfinger.net/rel/avatar",
                    "href": format!("https://{}/{}", &site.domain, &site.avatar_src),
                    "type": "image/jpeg"
                },
                {
                    "rel": "self",
                    "href": format!("https://{}", &site.domain),
                    "type": "text/html"
                },
                {
                    "rel": "http://webfinger.net/rel/profile-page",
                    "href": format!("https://{}", &site.domain),
                    "type": "text/html"
                },
                {
                    "rel": "self",
                    "href": format!("https://{}/ap/actor", &site.domain),
                    "type": "application/activity+json"
                },
            ]
        })
        .to_string();

        Ok(Content(content_type, doc))
    }
    else {
        Err(NotFound(String::from("Couldn't find requested user")))
    }
}

#[get("/.well-known/host-meta")]
pub fn host_meta(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "xrd+xml");
    let doc = format!(
        r#"<?xml version="1.0"?>
<XRD xmlns="http://docs/oasis-open.org/ns/xri/xrd-1.0">
    <Link rel="lrdd" type="application/xrd+xml" template="{domain}/.well-known/webfinger?resource={{uri}}" />
</XRD>"#,
        domain = site.domain
    );

    Content(content_type, doc)
}

#[get("/.well-known/nodeinfo")]
pub fn nodeinfo_jrd(site: State<Site>) -> Content<String> {
    let content_type = ContentType::new("application", "jrd+json");
    let doc = json!({
        "links": [
            {
                "href": format!("{host}/nodeinfo/2.0", host=&site.h_card.url),
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0"
            },
            {
                "href": format!("{host}/nodeinfo/2.1", host=&site.h_card.url),
                "rel": "http://nodeinfo.diaspora.software/ns/schema/2.1"
            }
        ]
    })
    .to_string();

    Content(content_type, doc)
}

#[get("/nodeinfo/<version>")]
pub fn nodeinfo(site: State<Site>, version: String) -> Content<String> {
    // TODO: Make return type a result and return 404 if version not 2.0 | 2.1
    let content_type = ContentType::with_params(
        "application",
        "json",
        ("profile", "http://nodeinfo.diaspora.software/ns/schema/2.0#,"),
    );
    let mut doc = json!({
        "version": version,
        "software": {
            "name": env!("CARGO_PKG_NAME"),
            "version": env!("CARGO_PKG_VERSION"),
        },
        "protocols": ["webmention"],
        "services": {
            "inbound": [],
            "outbound": []
        },
        "openRegistrations": false,
        "usage": {
            "users": {
                "total": 1,
            },
            "localPosts": utils::num_posts(&site)
        },
        "metadata": {
            "nodeName": &site.name,
            "nodeDescription": &site.description,
            "software": {
                "homepage": env!("CARGO_PKG_HOMEPAGE"),
                "repository": env!("CARGO_PKG_REPOSITORY"),
            }
        }
    });

    if version == "2.1" {
        doc["software"]["repository"] = json!(env!("CARGO_PKG_REPOSITORY"));
    }

    Content(content_type, doc.to_string())
}
