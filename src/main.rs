// #![doc(html_favicon_url = "")]
// #![doc(html_logo_url = "")]
#![feature(decl_macro, proc_macro_hygiene)]

#[macro_use]
extern crate pest_derive;

use dotenv::dotenv;
use log4rs;
use rocket_contrib::{serve::StaticFiles, templates::Template};

pub mod activity_pub;
pub mod micropub;
pub mod routes;
pub mod settings;
pub mod setup;
pub mod site;
pub mod utils;
pub mod webmentions;

fn main() {
    dotenv().ok();
    log4rs::init_file("log4rs.yml", Default::default()).unwrap();

    rocket::ignite()
        .attach(setup::attach_fairing())
        .attach(Template::fairing())
        .mount("/", routes::index::routes())
        .mount("/", routes::media::routes())
        .mount("/", routes::search::routes())
        .mount("/", routes::well_known::routes())
        .mount("/micropub", routes::micropub::routes())
        .mount("/ap", routes::activity_pub::routes())
        .mount("/notes", routes::notes::routes())
        .mount("/articles", routes::articles::routes())
        .mount("/photos", routes::photos::routes())
        .mount("/likes", routes::likes::routes())
        .mount("/boosts", routes::boosts::routes())
        // should static files be served from a absolute path, eg. /var/www/public?
        .mount("/public", StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/public")))
        .launch();
}
