use rocket::{
    outcome::IntoOutcome,
    request::{self, FromRequest, Request},
    FromForm,
};

#[derive(FromForm)]
pub struct Settings {
    pub theme: String,
}

impl<'a, 'r> FromRequest<'a, 'r> for Settings {
    type Error = &'static str;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Settings, &'static str> {
        request
            .cookies()
            .get("theme")
            .map(|cookie| cookie.value())
            .or_else(|| Some("default"))
            .map(|theme| Settings {
                theme: theme.to_string(),
            })
            .or_forward(())
    }
}
