use crate::{
    micropub::post::{PostType, PostView},
    site::Site,
};
use log::debug;
use rocket::FromForm;
use serde_json;
use std::{
    fs::{self, File},
    io::Write,
    path::Path,
};
use time;

#[derive(FromForm, Debug)]
pub struct IndexQuery {
    pub page: usize,
}

fn get_optional_value(json: &serde_json::Value, key: &str) -> Option<String> {
    json[key].as_str().map(|s| s.to_string())
}

// TODO: Consider taking `json` as a reference, &serde_json::Value
fn parse_entry_json(json: serde_json::Value, site: &Site) -> Result<PostView, String> {
    let post_type_str = json["post_type"].as_str().unwrap_or("none");
    let content = get_optional_value(&json, "content").expect("Couldn't get post content");
    let published = get_optional_value(&json, "published").expect("Couldn't get post timestamp");
    let mut tags: Vec<String> = json["tags"]
        .as_array()
        .unwrap_or(&vec![])
        .iter()
        .map(|t| t.as_str().unwrap().to_string())
        .collect();

    site.post()
        .content(Some(content))
        .name(get_optional_value(&json, "name"))
        .summary(get_optional_value(&json, "summary"))
        .published(published)
        .slug(get_optional_value(&json, "slug"))
        .tags(&mut tags)
        .in_reply_to(get_optional_value(&json, "in_reply_to"))
        .repost_of(get_optional_value(&json, "repost_of"))
        .like_of(get_optional_value(&json, "like_of"))
        .photo(get_optional_value(&json, "src"))
        .hydrate(post_type_str)
}

// TODO: Consider taking `post_view` as a reference instead: `&PostView`
pub fn store_post_view(post_view: &PostView, site: &Site) -> String {
    let path = Path::new(&site.content_dir).join(format!("{}.json", &post_view.slug));
    let mut file = File::create(path).expect("Couldn't open file for writing");
    let post_string = serde_json::to_string_pretty(post_view).expect("Couldn't serialize post for writing");
    file.write_all(post_string.as_bytes()).expect("Couldn't write post");
    post_view.url.clone()
}

pub fn num_posts(site: &Site) -> usize {
    let content_path = Path::new(&site.content_dir);
    fs::read_dir(content_path)
        .unwrap_or_else(|_| panic!("Can't read from content directory, {}", &site.content_dir))
        .filter(|result| result.is_ok())
        .count()
}

fn load_all(site: &Site) -> Vec<PostView> {
    let content_path = Path::new(&site.content_dir);
    let mut ret: Vec<PostView> = fs::read_dir(content_path)
        .unwrap_or_else(|_| panic!("Can't read from content directory, {}", &site.content_dir))
        .filter(|result| result.is_ok())
        .map(|result| {
            let path_buf = result.unwrap().path();
            let path = path_buf.as_path();
            fs::read_to_string(path)
        })
        .filter_map(|result| {
            if result.is_err() {
                let err = result.unwrap_err();
                debug!("Error reading post: {}", err);
                None
            }
            else {
                result.ok()
            }
        })
        .filter_map(|c| {
            let json = serde_json::from_str(&c);
            if json.is_err() {
                let err = json.unwrap_err();
                debug!("Error reading post: {}", err);
                None
            }
            else {
                json.ok()
            }
        })
        .map(|json| parse_entry_json(json, site))
        .filter_map(|result| result.ok())
        .collect();
    ret.sort_unstable_by(|a, b| {
        let time_a = time::strptime(&a.published, &site.date_format).unwrap();
        let time_b = time::strptime(&b.published, &site.date_format).unwrap();
        time_b.cmp(&time_a)
    });
    ret
}

pub fn load_entries(_page: usize, site: &Site) -> Vec<PostView> {
    load_all(site)
        .into_iter()
        .filter(|entry| entry.post_type != PostType::Like)
        .collect()
}

pub fn load_entries_by_type(post_type: PostType, _page: usize, site: &Site) -> Vec<PostView> {
    load_all(site)
        .into_iter()
        .filter(|entry| entry.post_type == post_type)
        .collect()
}

pub fn load_entry_by_slug(slug: &str, site: &Site) -> PostView {
    let path = Path::new(&site.content_dir).join(format!("{}.json", slug));
    let entry_path = path.as_path();
    let content = fs::read_to_string(entry_path).expect("Error reading post");

    let json = serde_json::from_str(&content).unwrap();
    parse_entry_json(json, site).unwrap()
}
