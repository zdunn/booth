use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct Properties {
    pub content: Option<Vec<String>>,
    pub name: Option<Vec<String>>,
    pub summary: Option<Vec<String>>,
    pub published: Option<Vec<String>>,
    pub updated: Option<Vec<String>>,
    pub category: Option<Vec<String>>,
    pub photo: Option<Vec<String>>,
    pub in_reply_to: Option<Vec<String>>,
    pub repost_of: Option<Vec<String>>,
    pub like_of: Option<Vec<String>>,
    pub syndication: Option<Vec<String>>,
    pub mp_syndicate_to: Option<Vec<String>>,
    pub mp_slug: Option<Vec<String>>,
}

/// Micropub request for creating a post in JSON format
///
/// # Examples
///
/// ```
/// POST /micropub HTTP/1.1
/// Host: example.com
/// Content-type: application/json
///
/// {
///     "type": "h-entry",
///     "name": "Title of a Post",
///     "summary": "An example post",
///     "content": "This is an example of the request used to create a post.",
///     "category": [ "example" ]
/// }
/// ```
///
/// # See Also
/// [h-entry](http:microformats.org/wiki/h-entry)
#[derive(Serialize, Deserialize, Debug)]
pub struct EntryRequest {
    #[serde(rename = "type")]
    pub h: Option<Vec<String>>,
    pub properties: Properties,
}
