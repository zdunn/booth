use pest::Parser;
use time;

#[derive(Parser)]
#[grammar = "booth.pest"]
pub struct PostParser;
pub struct Sanitizer;

impl Sanitizer {
    pub fn slugify_name(name: &str) -> String {
        name.trim()
            .to_lowercase()
            .replace(" ", "-")
            .replace(".", "-")
            .replace("_", "-")
    }

    pub fn format_published_date(published: Option<String>, date_format: &str) -> String {
        published.unwrap_or_else(|| time::strftime(date_format, &time::now()).expect("Couldn't format published date"))
    }

    pub fn parse_note(content: &str) -> (String, Vec<String>, Vec<String>) {
        let parse_result = PostParser::parse(Rule::note_content, content)
            .expect("unsuccessful parse")
            .next()
            .unwrap();
        let mut tags: Vec<String> = Vec::new();
        let mut links: Vec<String> = Vec::new();

        let output = parse_result
            .into_inner()
            .filter_map(|token| match token.as_rule() {
                Rule::hashtag => {
                    let mut inner_rule = token.into_inner();
                    let tag = inner_rule.next().unwrap().as_str();

                    tags.push(String::from(tag));
                    Some(format!(
                        " <a href=\"/tags/{}\" class=\"hashtag\" title=\"{} tag\">#{}</a> ",
                        tag, tag, tag
                    ))
                },
                Rule::link => {
                    let link = token.as_str();
                    links.push(link.to_string());
                    Some(format!(" <a href=\"{}\">{}</a> ", link, link))
                },
                Rule::newline => Some(String::from("<br>")),
                Rule::raw_text => Some(String::from(token.as_str())),
                _ => None,
            })
            .collect();

        (output, tags, links)
    }

    pub fn parse_article(content: &str) -> (String, Vec<String>) {
        let parse_result = PostParser::parse(Rule::article_content, content)
            .expect("unsuccessful parse")
            .next()
            .unwrap();
        let mut links: Vec<String> = Vec::new();

        let output = parse_result
            .into_inner()
            .filter_map(|token| match token.as_rule() {
                Rule::link => {
                    let link = token.as_str();
                    links.push(link.to_string());
                    Some(format!(" <a href=\"{}\">{}</a> ", link, link))
                },
                Rule::newline => Some(String::from("<br>")),
                Rule::raw_text => Some(String::from(token.as_str())),
                _ => None,
            })
            .collect();

        (output, links)
    }
}

#[cfg(test)]
mod tests {
    use crate::micropub::sanitize::Sanitizer;

    #[test]
    fn test_slugify_name() {
        assert_eq!(
            String::from("name"),
            Sanitizer::slugify_name("name"),
            "Sanitizer::slugify_name should not modify a string with only alphanumeric characters"
        );

        assert_eq!(
            String::from("test-post"),
            Sanitizer::slugify_name("test_post"),
            "Sanitizer::slugify_name should replace separator characters(_, <space>, <period>) with a hypen(-)"
        );

        assert_eq!(
            String::from("test-post"),
            Sanitizer::slugify_name("test.post"),
            "Sanitizer::slugify_name should replace separator characters(_, <space>, <period>) with a hypen(-)"
        );

        assert_eq!(
            String::from("test-post"),
            Sanitizer::slugify_name("test post"),
            "Sanitizer::slugify_name should replace separator characters(_, <space>, <period>) with a hypen(-)"
        );

        // TODO: Add more tests here
    }
}
