use rocket::FromForm;
use serde_derive::{Deserialize, Serialize};

/// Micropub request for creating a post in fomr-encoded format
///
/// # Examples
///
/// ```
/// POST /micropub HTTP/1.1
/// Host: example.com
/// Content-type: application/json
///
/// "h=entry&name=Title%20of%20a%20Post&summary=An%20example%20post&content=This%20is%20an%20example%20of%20the%20request%20used%20to%20create%20a%20post."
/// ```
///
/// # See Also
/// [h-entry](http:microformats.org/wiki/h-entry)
#[derive(Serialize, Deserialize, Debug, FromForm)]
pub struct EntryRequest {
    /// The microformat type of the object to create
    pub h: Option<String>,
    /// Entry name/title
    pub name: Option<String>,
    /// short summary
    pub summary: Option<String>,
    /// full content of the entry
    pub content: Option<String>,
    /// Timestamp for when the entry was published
    pub published: Option<String>,
    /// Timestamp for when the entry was updated
    pub updated: Option<String>,
    /// Entry categories/tags
    // Can't be a Vec currently; see below thread for details
    // https://github.com/SergioBenitez/Rocket/issues/205
    // pub category: Option<Vec<String>>,
    pub category: Option<String>,
    pub photo: Option<String>,
    #[form(field = "in-reply-to")]
    pub in_reply_to: Option<String>,
    #[form(field = "repost-of")]
    pub repost_of: Option<String>,
    #[form(field = "like-of")]
    pub like_of: Option<String>,
    pub syndication: Option<String>,
    #[form(field = "mp-syndicate-to")]
    pub mp_syndicate_to: Option<String>,
    #[form(field = "mp-slug")]
    pub mp_slug: Option<String>,
}
