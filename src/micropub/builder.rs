use crate::micropub::{
    post::{PostType, PostView},
    sanitize::Sanitizer,
};
use std::{
    boxed::Box,
    collections::hash_map::DefaultHasher,
    hash::{Hash, Hasher},
};

fn hash(opt: &Option<String>) -> String {
    let mut hasher = DefaultHasher::new();
    opt.clone().unwrap().hash(&mut hasher);
    hasher.finish().to_string()
}

pub struct PostBuilder<'a> {
    pub author: &'a str,
    pub host: &'a str,
    pub date_format: &'a str,
    pub content: Option<String>,
    pub name: Option<String>,
    pub summary: Option<String>,
    pub published: Option<String>,
    pub slug: Option<String>,
    pub tags: Vec<String>,
    pub in_reply_to: Option<String>,
    pub repost_of: Option<String>,
    pub like_of: Option<String>,
    pub photo: Option<String>,
    pub post_type: Option<PostType>,
    pub url: Option<String>,
}

pub struct InnerBuilder<'a> {
    pub author: &'a str,
    pub content: Option<String>,
    pub name: Option<String>,
    pub summary: Option<String>,
    pub published: String,
    pub slug: String,
    pub tags: Vec<String>,
    pub in_reply_to: Option<String>,
    pub repost_of: Option<String>,
    pub like_of: Option<String>,
    pub photo: Option<String>,
    pub post_type: PostType,
    pub url: String,
}

type PostSuccess<'a> = (Box<PostView>, Option<Vec<String>>);
impl<'a> PostBuilder<'a> {
    pub fn new(author: &'a str, host: &'a str, date_format: &'a str) -> PostBuilder<'a> {
        PostBuilder {
            author,
            host,
            date_format,
            content: None,
            name: None,
            summary: None,
            published: None,
            slug: None,
            tags: Vec::new(),
            in_reply_to: None,
            repost_of: None,
            like_of: None,
            photo: None,
            post_type: None,
            url: None,
        }
    }

    pub fn content<S>(mut self, content: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.content = content.map(|s| s.into());
        self
    }

    pub fn name<S>(mut self, name: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.name = name.map(|s| s.into());
        self
    }

    pub fn summary<S>(mut self, summary: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.summary = summary.map(|s| s.into());
        self
    }

    pub fn published<S>(mut self, published: S) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.published = Some(published.into());
        self
    }

    pub fn slug<S>(mut self, slug: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.slug = slug.map(|s| s.into());
        self
    }

    pub fn tags(mut self, tags: &mut Vec<String>) -> PostBuilder<'a> {
        self.tags.append(tags);
        self
    }

    pub fn in_reply_to<S>(mut self, in_reply_to: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.in_reply_to = in_reply_to.map(|i| i.into());
        self
    }

    pub fn repost_of<S>(mut self, repost_of: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.repost_of = repost_of.map(|i| i.into());
        self
    }

    pub fn like_of<S>(mut self, like_of: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.like_of = like_of.map(|i| i.into());
        self
    }

    pub fn photo<S>(mut self, photo: Option<S>) -> PostBuilder<'a>
    where
        S: Into<String>,
    {
        self.photo = photo.map(|i| i.into());
        self
    }

    pub fn hydrate(self, post_type_str: &str) -> Result<PostView, String> {
        let content = self.content.unwrap();
        let name = self.name.unwrap();
        let slug = self.slug.unwrap();
        let published = self.published.unwrap();

        let post_type = PostType::from(post_type_str);
        if let PostType::Unsupported(unsupported_type) = post_type {
            return Err(format!("Unsupported post type {}", unsupported_type));
        }

        let url = format!("{host}/{type}s/{slug}", host=self.host, type=post_type_str, slug=slug);
        let post = PostView {
            post_type,
            author: self.author.to_string(),
            url,
            published,
            content,
            name,
            summary: self.summary,
            photo: self.photo.map(|photo_slug| format!("/public/images/{}", photo_slug)),
            tags: self.tags,
            slug,
            in_reply_to: self.in_reply_to,
            repost_of: self.repost_of,
            like_of: self.like_of,
        };
        Ok(post)
    }

    pub fn validate(self) -> Result<InnerBuilder<'a>, &'static str> {
        if self.like_of.is_some() || self.repost_of.is_some() {
            let (target_url, path, post_type) = if self.like_of.is_some() {
                (&self.like_of, "likes", PostType::Like)
            }
            else {
                (&self.repost_of, "boosts", PostType::Boost)
            };
            let slug = hash(target_url);
            let url = format!("{host}/{path}/{slug}", host = self.host, path = path, slug = &slug);

            return Ok(InnerBuilder {
                post_type,
                url,
                slug,
                photo: self.photo.map(|photo_slug| format!("/public/images/{}", photo_slug)),
                published: Sanitizer::format_published_date(self.published, self.date_format),
                author: self.author,
                content: self.content,
                name: self.name,
                summary: self.summary,
                tags: self.tags,
                in_reply_to: self.in_reply_to,
                repost_of: self.repost_of,
                like_of: self.like_of,
            });
        }

        let slug = self
            .slug
            .as_ref()
            .or_else(|| self.name.as_ref())
            .map(|s| &**s)
            .map(|s| Sanitizer::slugify_name(s))
            .unwrap_or_else(|| hash(&self.content));

        if self.content.is_none() {
            return Err("Missing 'content' property");
        }

        if self.photo.is_some() {
            let post_type = PostType::Photo;
            if self.name.is_none() {
                return Err("'name' property is required for photo posts");
            }

            let url = format!("{host}/photos/{slug}", host = self.host, slug = slug);
            return Ok(InnerBuilder {
                post_type,
                url,
                slug,
                published: Sanitizer::format_published_date(self.published, self.date_format),
                photo: self.photo,
                author: self.author,
                content: self.content,
                name: self.name,
                summary: self.summary,
                tags: self.tags,
                in_reply_to: self.in_reply_to,
                repost_of: self.repost_of,
                like_of: self.like_of,
            });
        }

        let has_name = self.name.as_ref().map_or(false, |n| !n.is_empty());
        let has_summary = self.summary.as_ref().map_or(false, |s| !s.is_empty());
        let (post_type, path) = if has_summary && has_name {
            (PostType::Article, "articles")
        }
        else {
            (PostType::Note, "notes")
        };
        let url = format!("{host}/{path}/{slug}", host = self.host, path = path, slug = slug);

        Ok(InnerBuilder {
            post_type,
            url,
            slug,
            published: Sanitizer::format_published_date(self.published, self.date_format),
            photo: self.photo,
            author: self.author,
            content: self.content,
            name: self.name,
            summary: self.summary,
            tags: self.tags,
            in_reply_to: self.in_reply_to,
            repost_of: self.repost_of,
            like_of: self.like_of,
        })
    }
}

impl<'a> InnerBuilder<'a> {
    pub fn build(self) -> PostSuccess<'a> {
        let InnerBuilder {
            author,
            content,
            name,
            summary,
            published,
            slug,
            tags,
            in_reply_to,
            repost_of,
            like_of,
            photo,
            post_type,
            url,
            ..
        } = self;
        let raw_content = content.unwrap_or_else(String::new);

        let (post_type, content, summary, tags, links) = match post_type {
            PostType::Photo => {
                let summary = summary.or_else(|| Some(String::from("")));
                (PostType::Photo, raw_content, summary, tags, None)
            },
            PostType::Article => {
                let summary = summary.or_else(|| Some(raw_content.lines().take(3).collect()));
                let (content, mut links) = Sanitizer::parse_article(&raw_content);
                if in_reply_to.is_some() {
                    links.push(in_reply_to.clone().unwrap());
                }
                (PostType::Article, content, summary, tags, Some(links))
            },
            PostType::Note => {
                let (content, tags, mut links) = Sanitizer::parse_note(&raw_content);
                if in_reply_to.is_some() {
                    links.push(in_reply_to.clone().unwrap());
                }
                (PostType::Note, content, None, tags, Some(links))
            },
            pt @ PostType::Like | pt @ PostType::Boost => {
                let target = if pt == PostType::Like {
                    like_of.clone()
                }
                else {
                    repost_of.clone()
                };
                (pt, raw_content, None, Vec::new(), Some(vec![target.unwrap()]))
            },
            PostType::Unsupported(pt) => panic!("Can't build post with unsupported type: {}", pt),
        };
        let post = PostView {
            post_type,
            author: author.to_string(),
            url,
            published,
            content,
            name: name.unwrap_or_else(|| String::from("")),
            summary,
            photo: photo.map(|photo_slug| format!("/public/images/{}", photo_slug)),
            tags,
            slug,
            in_reply_to,
            repost_of,
            like_of,
        };
        (Box::new(post), links)
    }
}

#[cfg(test)]
mod tests {
    use crate::micropub::{builder::PostBuilder, post::PostType};

    fn post_builder() -> PostBuilder<'static> {
        PostBuilder::new("Tester", "example.com", "YYYY-MM-dd")
    }

    #[test]
    fn test_validation_note_only_content() {
        let post_result = post_builder()
            .content(Some("This is some post content".to_string()))
            .validate();

        assert!(post_result.is_ok(), "`validate` should return an Ok value");
        let builder = post_result.unwrap();
        assert_eq!(
            PostType::Note,
            builder.post_type,
            "A post with just content should be validated as a PostType::Note"
        );
    }

    #[test]
    fn test_validation_note_reply() {
        let post_result = post_builder()
            .content(Some("This is a cogent reply".to_string()))
            .in_reply_to(Some("https://fake.blog/interesting-article".to_string()))
            .validate();

        assert!(post_result.is_ok(), "`validate` should return an Ok value");
        let builder = post_result.unwrap();
        assert_eq!(
            PostType::Note,
            builder.post_type,
            "A reply post should be validated as a PostType::Note"
        );
        assert!(builder.in_reply_to.is_some());
    }

    #[test]
    fn test_validation_note_like() {
        let post_result = post_builder()
            .like_of(Some("https://fake.blog/interesting-article".to_string()))
            .validate();

        assert!(post_result.is_ok(), "`validate` should return an Ok value");
        let builder = post_result.unwrap();
        assert_eq!(
            PostType::Like,
            builder.post_type,
            "A like post should be validated as a PostType::Like"
        );
        assert!(builder.like_of.is_some());
    }

    #[test]
    fn test_validation_note_repost() {
        let post_result = post_builder()
            .repost_of(Some("https://fake.blog/interesting-article".to_string()))
            .validate();

        assert!(post_result.is_ok(), "`validate` should return an Ok value");
        let builder = post_result.unwrap();
        assert_eq!(
            PostType::Boost,
            builder.post_type,
            "A repost should be validated as a PostType::Boost"
        );
        assert!(builder.repost_of.is_some());
    }
}
