//! # MicroPub Syndication
//!
//! `syndication` is a collection of utilities for communicating the user's available syndication options.
use serde_derive::Serialize;

/// Details about an external service
#[derive(Serialize)]
pub struct SyndicationService {
    /// the name of the service displayed to the user
    pub name: String,
    /// the url of the service
    pub url: Option<String>,
    /// a url for an image displayed to the user
    pub photo: Option<String>,
}

/// Details about a user's account at an external service
#[derive(Serialize)]
pub struct SyndicationUser {
    /// The user's name on the external account
    pub name: String,
    /// The url of the external account
    pub url: Option<String>,
    /// The url of the external account's avatar
    pub photo: Option<String>,
}

/// A micropub syndication target.
#[derive(Serialize)]
pub struct SyndicationTarget {
    /// A unique identifier not visible to end user,
    /// usually the service's url
    pub uid: String,
    /// Name of the service to be show to the user
    /// should be unambiguous so that two destinations on the same service are distinguasable
    pub name: String,
    /// Additional details about the service
    /// See [SyndicationService](struct.SyndicationService) for more.
    pub service: Option<SyndicationService>,
    /// Details about the user's account on the service
    /// See [SyndicationUser](struct.SyndicationService) for more.
    pub user: Option<SyndicationUser>,
}

impl SyndicationTarget {
    /// Constructs a new `SyndicationTarget` with a url and name.
    /// The syndication target will not have an associated user account or extra details
    /// Use `service`/`user` methods to add a `SyndicationService`/`SyndicationUser`
    ///
    /// # Examples
    ///
    /// ```
    /// let service_url = String::from("https://twitter.com");
    /// let service_name = String::from("Twitter");
    /// let syndication_target = SyndicationTarget::new(service_url, service_name);
    /// ```
    pub fn new(uid: String, name: String) -> SyndicationTarget {
        SyndicationTarget {
            uid,
            name,
            service: None,
            user: None,
        }
    }

    /// Add a `SyndicationService` to the syndication target
    pub fn service(mut self, name: String, url: Option<String>, photo: Option<String>) -> SyndicationTarget {
        self.service = Some(SyndicationService { name, url, photo });
        self
    }

    /// Add a `SyndicationUser` to the syndication target
    pub fn user(mut self, name: String, url: Option<String>, photo: Option<String>) -> SyndicationTarget {
        self.user = Some(SyndicationUser { name, url, photo });
        self
    }
}
