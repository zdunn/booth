//! # Micropub
//!
//! This module contains utilities for implementing the Micropub spec.

use rocket::FromForm;
use serde_derive::Serialize;

pub mod builder;
pub mod jf2;
pub mod mf2;
pub mod post;
pub mod responders;
pub mod sanitize;
pub mod syndication;

/// Micropub config query parameters
#[derive(FromForm)]
pub struct ConfigQuery {
    pub q: String,
    pub url: Option<String>,
}

/// Representation of a h-card microformat
#[derive(Debug, Serialize)]
pub struct Card {
    pub name: String,
    pub url: String,
    pub nickname: String,
    pub email: Option<String>,
}

impl Card {
    pub fn new<S>(name: S, url: S, nickname: S, email: Option<S>) -> Card
    where
        S: Into<String>,
    {
        Card {
            name: name.into(),
            url: url.into(),
            nickname: nickname.into(),
            email: email.map(|s| s.into()),
        }
    }
}
