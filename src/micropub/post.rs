use serde_derive::Serialize;
use std::convert::From;

#[derive(PartialEq, Serialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum PostType {
    Note,
    Article,
    Photo,
    Boost,
    Like,
    Unsupported(String),
}

impl From<&str> for PostType {
    fn from(s: &str) -> PostType {
        match s {
            "note" => PostType::Note,
            "article" => PostType::Article,
            "photo" => PostType::Photo,
            "repost" | "boost" => PostType::Boost,
            "like" => PostType::Like,
            other => PostType::Unsupported(other.to_string()),
        }
    }
}

#[derive(Serialize, Debug)]
pub struct PostView {
    pub post_type: PostType,
    pub url: String,
    pub published: String,
    pub content: String,
    pub name: String,
    pub summary: Option<String>,
    pub author: String,
    pub photo: Option<String>,
    pub tags: Vec<String>,
    pub slug: String,
    pub in_reply_to: Option<String>,
    pub repost_of: Option<String>,
    pub like_of: Option<String>,
}
