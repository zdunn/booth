use log::error;
use rocket::{
    http::{ContentType, Status},
    request::Request,
    response::{self, Responder, Response},
};
use serde_derive::Serialize;
use serde_json::{self, json};
use std::io::Cursor;

#[derive(Serialize)]
pub struct MicropubError {
    error: String,
    error_description: String,
}

#[derive(Debug)]
pub enum MicropubResponse {
    MissingContent,
    NotImplemented,
    UnsupportedType(String),
    UnsupportedQuery(String),
    BadRequest(String),
    Created(String),
}

fn micropub_error_json<S>(error: S, error_description: S) -> Option<serde_json::Value>
where
    S: Into<String>,
{
    let json = json!({
        "error": error.into(),
        "error_description": error_description.into(),
    });
    Some(json)
}

impl<'r> Responder<'r> for MicropubResponse {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        let mut builder = Response::build();

        let (status, body) = match self {
            MicropubResponse::MissingContent => (
                Status::BadRequest,
                micropub_error_json("invalid_request", "Missing 'content' property"),
            ),
            MicropubResponse::NotImplemented => (
                Status::NotImplemented,
                micropub_error_json("Not Implemented", "I haven't gottent to this yet"),
            ),
            MicropubResponse::UnsupportedType(h_type) => (
                Status::BadRequest,
                micropub_error_json("invalid_request", &format!("Unsupported h-type: {}", h_type)),
            ),
            MicropubResponse::UnsupportedQuery(query) => (
                Status::BadRequest,
                micropub_error_json("invalid_request", &format!("Unsupported query type: {}", query)),
            ),
            MicropubResponse::BadRequest(err) => (
                Status::BadRequest,
                micropub_error_json(String::from("invalid_request"), err),
            ),
            MicropubResponse::Created(url) => {
                builder.raw_header("Location", url);
                (Status::Created, None)
            },
        };

        if let Some(json) = body {
            let error_description = json["error_description"].as_str().unwrap();
            error!("{}", error_description);

            builder
                .header(ContentType::JSON)
                .sized_body(Cursor::new(json.to_string()));
        }

        builder.status(status).ok()
    }
}
