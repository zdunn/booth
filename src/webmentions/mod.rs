use log::info;
use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Webmention {
    source: String,
    target: String,
}

pub struct WebmentionBuilder {
    pub source: String,
    pub targets: Vec<String>,
}

impl WebmentionBuilder {
    pub fn new(source: String) -> WebmentionBuilder {
        WebmentionBuilder {
            source,
            targets: Vec::new(),
        }
    }

    pub fn target(mut self, target: String) -> WebmentionBuilder {
        self.targets.push(target);
        self
    }

    pub fn targets(mut self, targets: &mut Vec<String>) -> WebmentionBuilder {
        self.targets.append(targets);
        self
    }

    pub fn send(self) {
        // TODO: loop through targets, find webmention endpoint, and send mention
        for target in self.targets {
            // TODO: find webention endpoint
            // let webmention_endpoint = ...

            /*
            let webmention = Webmention {
                source: self.source.clone(),
                target,
            };
            */

            // POST `webmention` to webmention_endpoint
            info!("{}\t{}", &self.source, target);
        }
    }
}
