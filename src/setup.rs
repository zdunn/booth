use crate::{micropub::Card, site::Site};
use rocket::fairing::AdHoc;
use std::{env, path::Path};

pub fn attach_fairing() -> AdHoc {
    AdHoc::on_attach("Attach Fairing", |rocket| {
        let name = env::var("BOOTH_NAME").expect("Missing 'NAME' configuration parameter");
        let username = env::var("BOOTH_USERNAME").expect("Missing 'USERNAME' configuration parameter");
        let site_name = env::var("BOOTH_SITE_NAME").unwrap_or_else(|_err| name.clone());
        let site_name_short = env::var("BOOTH_SITE_NAME_SHORT").unwrap_or_else(|_err| username.clone());
        let domain = env::var("BOOTH_DOMAIN").expect("Missing 'DOMAIN' configuration parameter");
        let host = env::var("BOOTH_HOST").unwrap_or_else(|_err| domain.clone());
        let description = env::var("BOOTH_DESCRIPTION").expect("Missing 'DESCRIPTION' configuration parameter");
        let date_format = env::var("BOOTH_DATE_FORMAT").expect("Missing 'DATE_FORMAT' configuration parameter");
        let locale = env::var("BOOTH_LOCALE").unwrap_or_else(|_err| String::from("en-US"));
        let email = env::var("BOOTH_EMAIL").ok();
        let public_dir = env::var("BOOTH_PUBLIC_DIR").unwrap_or_else(|_err| String::from("public"));
        let content_dir = env::var("BOOTH_CONTENT_DIR").unwrap_or_else(|_err| String::from("content"));
        let page_size = env::var("BOOTH_PAGE_SIZE")
            .unwrap_or_else(|_err| String::from("10"))
            .parse::<usize>()
            .expect("Error parsing PAGE_SIZE configuration parameter");
        let avatar = env::var("BOOTH_AVATAR").expect("Missing 'AVATAR' configuration parameter");
        let src_path = Path::new(&public_dir).join("images").join(avatar);
        let avatar_src = src_path.to_str().unwrap().to_string();
        let rel_me_links = env::var("BOOTH_REL_ME_LINKS")
            .unwrap_or_else(|_err| String::new())
            .split(',')
            .map(|s| s.trim().to_string())
            .collect();

        // TODO: Read the ${public_dir}/css/themes directory for theme stylesheets
        let site = Site {
            h_card: Card::new(name, host, username, email),
            name: site_name,
            short_name: site_name_short,
            domain,
            description,
            date_format,
            locale,
            public_dir,
            content_dir,
            page_size,
            avatar_src,
            rel_me_links,
            themes: vec![String::from("default"), String::from("solarized-dark"), String::from("typewolf")],
            features: check_features(),
        };

        Ok(rocket.manage(site))
    })
}

fn check_features() -> Vec<String> {
    let mut features = Vec::new();

    if cfg!(feature = "search") {
        features.push(String::from("search"));
    }

    features
}
