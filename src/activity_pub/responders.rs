use log::error;
use rocket::{
    http::{ContentType, Status},
    request::Request,
    response::{self, Responder, Response},
};
use std::io::Cursor;

#[derive(Debug)]
pub enum APResponse {
    NotImplemented,
    BadRequest(String),
    Created(String),
}

impl<'r> Responder<'r> for APResponse {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        let mut builder = Response::build();

        let (status, body) = match self {
            APResponse::NotImplemented => (
                Status::NotImplemented,
                Some("I haven't gottent to this yet".to_string()),
            ),
            APResponse::BadRequest(err) => (Status::BadRequest, Some(err)),
            APResponse::Created(url) => {
                builder.raw_header("Location", url);
                (Status::Created, None)
            },
        };

        if let Some(err) = body {
            error!("{}", err);

            builder.header(ContentType::JSON).sized_body(Cursor::new(err));
        }

        builder.status(status).ok()
    }
}
