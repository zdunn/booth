use crate::{
    micropub::{builder::PostBuilder, post::PostView, Card},
    settings::Settings,
};
use rocket_contrib::templates::Template;
use serde_derive::Serialize;
use std::collections::HashMap;
use tera::Context;

/// Struct representing a Booth.pub site
#[derive(Debug, Serialize)]
pub struct Site {
    pub h_card: Card,
    pub name: String,
    pub short_name: String,
    pub domain: String,
    pub description: String,
    pub date_format: String,
    pub locale: String,
    pub public_dir: String,
    pub content_dir: String,
    pub page_size: usize,
    pub avatar_src: String,
    pub rel_me_links: Vec<String>,
    pub themes: Vec<String>,
    pub features: Vec<String>,
}

impl Site {
    /// Creates a Tera context with the following properties:
    /// * name - user's name
    /// * username - user's nickname
    /// * domain - site's domain
    /// * host - domain plus scheme and port if it's not 80
    /// * site_name - name of the website
    /// * locale - IETF language tag
    /// * avatar_src - src url for the user's avatar
    pub fn new_context(&self) -> HashMap<&str, &str> {
        let mut context = HashMap::new();
        context.insert("name", self.h_card.name.as_ref());
        context.insert("username", self.h_card.nickname.as_ref());
        context.insert("host", self.h_card.url.as_ref());
        context.insert("site_name", self.name.as_ref());
        context.insert("domain", self.domain.as_ref());
        context.insert("date_format", self.date_format.as_ref());
        context.insert("locale", self.locale.as_ref());
        context.insert("avatar_src", self.avatar_src.as_ref());
        context
    }

    pub fn post(&self) -> PostBuilder {
        PostBuilder::new(&self.h_card.name, &self.h_card.url, &self.date_format)
    }

    pub fn view(&self) -> ViewBuilder {
        ViewBuilder {
            hash_map: self.new_context(),
            title: None,
            slug: None,
            rel_me_links: &self.rel_me_links,
            description: None,
            description_default: self.description.clone(),
            posts: None,
            post: None,
            themes: &self.themes,
            features: &self.features,
            settings: None,
        }
    }
}

pub struct ViewBuilder<'a> {
    hash_map: HashMap<&'a str, &'a str>,
    title: Option<String>,
    slug: Option<String>,
    rel_me_links: &'a Vec<String>,
    description: Option<String>,
    description_default: String,
    posts: Option<Vec<PostView>>,
    post: Option<PostView>,
    themes: &'a Vec<String>,
    features: &'a Vec<String>,
    settings: Option<Settings>,
}

impl<'a> ViewBuilder<'a> {
    pub fn insert(mut self, key: &'a str, value: &'a str) -> ViewBuilder<'a> {
        if self.hash_map.contains_key(key) {
            panic!("Can't insert duplicate key '{}'", key);
        }

        self.hash_map.insert(key, value);
        self
    }

    pub fn title(mut self, title: &str) -> ViewBuilder<'a> {
        self.title = Some(title.to_string());
        self
    }

    pub fn slug(mut self, slug: &str) -> ViewBuilder<'a> {
        self.slug = Some(slug.to_string());
        self
    }

    pub fn description(mut self, description: &str) -> ViewBuilder<'a> {
        self.description = Some(description.to_string());
        self
    }

    pub fn posts(mut self, posts: Vec<PostView>) -> ViewBuilder<'a> {
        self.posts = Some(posts);
        self
    }

    pub fn post(mut self, post: PostView) -> ViewBuilder<'a> {
        self.post = Some(post);
        self
    }

    pub fn settings(mut self, settings: Settings) -> ViewBuilder<'a> {
        self.settings = Some(settings);
        self
    }

    pub fn render(self, template_path: &'static str) -> Template {
        let mut context = Context::new();
        context.insert("title", &self.title.expect("Title is required for a view context"));
        context.insert("slug", &self.slug.expect("Slug is required for a view context"));
        context.insert("rel_me_links", &self.rel_me_links);
        context.insert("description", &self.description.unwrap_or(self.description_default));
        context.insert("themes", &self.themes);
        context.insert("features", &self.features);

        context.insert(
            "theme",
            if let Some(ref settings) = self.settings {
                settings.theme.as_str()
            }
            else {
                "default"
            },
        );

        for (key, value) in self.hash_map.iter() {
            context.insert(key, value);
        }

        if let Some(posts) = self.posts {
            context.insert("posts", &posts);
        }

        if let Some(post) = self.post {
            context.insert("post", &post);
        }

        Template::render(template_path, context)
    }
}
